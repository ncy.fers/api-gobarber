module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgers',
  database: 'db_gobarber',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
