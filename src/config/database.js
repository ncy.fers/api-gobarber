module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: 'docker',
  port: '5430',
  database: 'db_gobarber',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
