import express from 'express';
import routes from './routes';
// const express = require('express')
// const routes = require('./routes')

import './database';

class App {
  constructor() {
    this.server = express();

    this.middelwares();
    this.routes();
  }

  middelwares() {
    this.server.use(express.json());
  }

  routes() {
    this.server.use(routes);
  }
}

// module.exports = new App().server
export default new App().server;
